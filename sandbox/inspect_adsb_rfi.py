# poetry run python -m sandbox.inspect_adsb_rfi.py /fred/oz002/baseband/meerkat/first_light/cbf_4ant/2016-04-28-14\:14\:19_0000000000000000.000000.dada

import sys

import psr_formats
import numpy as np
import matplotlib.pyplot as plt


def get_data(file_path: str) -> np.ndarray:
    dada_file = psr_formats.DADAFile(file_path).load_data()

    bw = float(dada_file["BW"])
    centre_freq = float(dada_file["FREQ"])
    nchan = int(dada_file["NCHAN"])

    freqs = np.linspace(centre_freq - bw/2, centre_freq + bw/2, nchan)

    return dada_file.data.copy(), freqs


def plot_data(data: np.ndarray, freqs: np.ndarray) -> None:

    nsamples = data.shape[0]

    imshow_kwargs = {
        "aspect": "auto",
        "origin": "lower",
        "extent": (0, nsamples, freqs[0], freqs[-1])
    }

    fig, axes = plt.subplots(1, 1)
    axes.imshow(np.abs(data.T), **imshow_kwargs)

    axes.set_xlabel("Sample Number")
    axes.set_ylabel("Frequency (MHz)")
    axes.set_title("Dynamic Spectrum")


def main():
    file_path = sys.argv[1]
    data_arr, freqs_arr = get_data(file_path)
    print("Done Loading data")
    plot_data(data_arr[:, :, 1], freqs_arr)
    print("Done plotting data")

    plt.show()


if __name__ == "__main__":
    main()
