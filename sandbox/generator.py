
def generator():

    yield 0

    val = yield 1

    yield 2

    yield val


if __name__ == "__main__":

    gen = generator()
    print(next(gen))
    print(next(gen))
    print(gen.send(10))
    print(next(gen))
