class IteratorClass:

    def __init__(self):
        self.attr = "hello"
        self._iterator = self._iterator_factory()

    def __iter__(self):
        return self

    def __next__(self):
        return next(self._iterator)

    def send(self, *args):
        return self._iterator.send(*args)

    def _iterator_factory(self):

        val = yield self.attr
        self.attr = val
        yield self.attr


if __name__ == "__main__":
    obj = IteratorClass()
    print(next(obj))
    print(obj.send("le pompidou"))
