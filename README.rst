PST RFI Injection
=================

Part of the RFI test case pipeline involves generating RFI profiles and adding them to existing datasets. ``rfi-injection`` is a standalone Python package for generating impulsive RFI profiles.

.. This repository is hosted `here <https://gitlab.com/dean-shaff/pst-rfi-injection>`_.


Usage
-----

Create a RFI profile:

.. code-block::

    poetry run python create_rfi_profile.py -o ./ -c /path/to/config.toml


Add a RFI profile to an existing DADA file:

.. code-block::

    poetry run python add_rfi.py -i path/to/data.dump -ri /path/to/rfi_profile.dump

Note that for ``add_rfi.py`` to work, the centre frequency and bandwidth of both the input file and the RFI profile file have to be the same.


Full Command Line Arguments
---------------------------

.. code-block::

  poetry run python create_rfi_profile.py -h
  usage: create_rfi_profile.py [-h] [-v] [-c CONFIG_FILE_PATH] [-o OUTPUT_DIR]
                               [-b BW] [-f CENTRE_FREQ] [-rb RFI_BW]
                               [-rf RFI_CENTRE_FREQ] [-ch NCHAN]
                               [-mw MEAN_WIDTH] [-sw SIGMA_WIDTH] [-ma MEAN_AMP]
                               [-sa SIGMA_AMP] [-ms MEAN_SPACING]
                               [-ss SIGMA_SPACING] [-p PLOT]
                               [-t TIME_OR_SAMPLES]

  optional arguments:
    -h, --help            show this help message and exit
    -v, --verbose
    -c CONFIG_FILE_PATH, --config-file CONFIG_FILE_PATH
    -o OUTPUT_DIR, --output-dir OUTPUT_DIR
    -b BW, --bandwidth BW
                          Specify bandwidth, in MHz
    -f CENTRE_FREQ, --centre-freq CENTRE_FREQ
                          Specify centre frequency, in MHz
    -rb RFI_BW, --rfi-bandwidth RFI_BW
                          Specify RFI bandwidth, in MHz
    -rf RFI_CENTRE_FREQ, --rfi-centre-freq RFI_CENTRE_FREQ
                          Specify RFI centre frequency, in MHz
    -ch NCHAN, --channels NCHAN
                          Specify number of RFI profile channels to generate
    -mw MEAN_WIDTH, --mean-width MEAN_WIDTH
                          Specify the mean width of RFI impulse, in seconds or
                          samples
    -sw SIGMA_WIDTH, --sigma-width SIGMA_WIDTH
                          Specify the standard deviation of the width of the RFI
                          impulse, in seconds or samples
    -ma MEAN_AMP, --mean-amp MEAN_AMP
                          Specify the mean amplitude of RFI impulses, in units
                          of voltage
    -sa SIGMA_AMP, --sigma-amp SIGMA_AMP
                          Specify the standard deviation of the amplitude of RFI
                          impulses, in units of voltage
    -ms MEAN_SPACING, -mean-spacing MEAN_SPACING
                          Specify the mean spacing between RFI impulses, in
                          samples or seconds
    -ss SIGMA_SPACING, --sigma-spacing SIGMA_SPACING
                          Specify the standard deviation of spacing between RFI
                          impulses, in samples or seconds
    -p PLOT, --plot PLOT  Plot an RFI profile
    -t TIME_OR_SAMPLES, --time TIME_OR_SAMPLES
                          Specify the number of seconds of data to produce

Any numerical entry command line arguments, with the exception of ``-ch``, can be specified with or without units. For example, if we wanted to specify the centre frequency in GHz instead of MHz we could pass ``-f 1.405GHz`` to ``create_rfi_profile.py``. Note that there can be no space between the number and accompanying unit. If no units are specified, the script assumes some default unit detailed in the command line argument description. In the case of time data, the default unit is samples.

Configuration
-------------

We can pass a configuration file to ``create_rfi_profile.py`` or ``add_rfi.py`` instead of specifying command line arguments. A example TOML configuration file:


.. code-block:: toml

  nchan = 128

  [mean_width]
  value = 1e-4
  unit = "s"

  [sigma_width]
  value = 4e-5
  unit = "s"

  [mean_spacing]
  value = 0.01
  unit = "s"

  [sigma_spacing]
  value = 0.004
  unit = "s"

  [rfi_bw]
  value = 2
  unit = "MHz"

  [rfi_centre_freq]
  value = 1400
  unit = "MHz"

  [mean_amp]
  value = 50 #3.5
  unit = "V"

  [sigma_amp]
  value = 0.5
  unit = "V"

Note that we do not have to specify units for the fields in the configuration. If we wanted the ``mean_spacing`` to be 40 samples, we could replace its entry with the following:


.. code-block:: toml

  ...

  mean_spacing = 40

  # [mean_spacing]
  # value = 0.01
  # unit = "s"
