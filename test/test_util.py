import logging
import unittest

from rfi_injection.util import auto_property


class Test_autoproperty(unittest.TestCase):

    def test_autoproperty(self):

        @auto_property(["prop1", "prop2"])
        class Decorated:
            def __init__(self):
                self._prop1 = "hello"
                self._prop2 = "je t'aime"

        obj = Decorated()
        self.assertTrue(obj.prop1 == "hello")
        obj.prop2 = "je ne t'aime pas"
        self.assertTrue(obj.prop2 == "je ne t'aime pas")


    def test_autoproperty_custom_setter(self):

        def setter(self, underscore_name, value):
            self._prop1 = "reset1"
            self._prop2 = "reset2"

        @auto_property(["prop1", "prop2"], custom_setter=setter)
        class Decorated:
            def __init__(self):
                self._prop1 = "hello"
                self._prop2 = "je t'aime"

        obj = Decorated()
        obj.prop1 = "hi"
        self.assertTrue(obj.prop1 == "reset1")
        self.assertTrue(obj.prop2 == "reset2")




if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    unittest.main()
