import os
import logging
import datetime

import psr_formats
import numpy as np
import astropy.units as u
import matplotlib.pyplot as plt

from rfi_injection import RFIProfile, NumQuantity
from rfi_injection.cli import create_parser, load_config, UnitAction

module_logger = logging.getLogger(__name__)


def create_rfi_profile(
    output_dir: str = "./",
    nchan: int = 1024,
    *,
    time_or_samples: NumQuantity,
    bw: NumQuantity,
    centre_freq: NumQuantity,
    rfi_bw: NumQuantity,
    rfi_centre_freq: NumQuantity,
    mean_width: NumQuantity,
    sigma_width: NumQuantity,
    mean_amp: NumQuantity,
    sigma_amp: NumQuantity,
    mean_spacing: NumQuantity,
    sigma_spacing: NumQuantity,
) -> np.ndarray:

    module_logger.debug(f"create_rfi_profile: time_or_samples={time_or_samples}")
    module_logger.debug(f"create_rfi_profile: bw={bw}")
    module_logger.debug(f"create_rfi_profile: centre_freq={centre_freq}")
    module_logger.debug(f"create_rfi_profile: rfi_bw={rfi_bw}")
    module_logger.debug(f"create_rfi_profile: rfi_centre_freq={rfi_centre_freq}")
    module_logger.debug(f"create_rfi_profile: mean_width={mean_width}")
    module_logger.debug(f"create_rfi_profile: sigma_width={sigma_width}")
    module_logger.debug(f"create_rfi_profile: mean_amp={mean_amp}")
    module_logger.debug(f"create_rfi_profile: sigma_amp={sigma_amp}")
    module_logger.debug(f"create_rfi_profile: mean_spacing={mean_spacing}")
    module_logger.debug(f"create_rfi_profile: sigma_spacing={sigma_spacing}")
    module_logger.debug(f"create_rfi_profile: output_dir={output_dir}")
    module_logger.debug(f"create_rfi_profile: nchan={nchan}")

    def param_generator(mean: NumQuantity, sigma: NumQuantity) -> np.ndarray:
        return lambda: np.random.randn(1) * sigma + mean

    def format_param(gen: RFIProfile, val: NumQuantity) -> str:
        return f"{gen_prof.sample_to_time(mean_width).value:.6f}"

    gen_prof = RFIProfile(
        bw, centre_freq, nchan, rfi_bw, rfi_centre_freq,
        param_generator(mean_width, sigma_width),
        param_generator(mean_amp, sigma_amp),
        param_generator(mean_spacing, sigma_spacing)
    )

    # profile = gen_prof(time_or_samples)
    profile = gen_prof(time_or_samples).astype(np.float32)

    rfi_file_name = f"rfi.profile.dump"
    rfi_file_path = os.path.join(output_dir, rfi_file_name)
    rfi_dada_file = psr_formats.DADAFile(rfi_file_path)
    rfi_dada_file["BW"] = f"{bw.to(u.MHz).value:.0f}"
    rfi_dada_file["FREQ"] = f"{centre_freq.to(u.MHz).value:.2f}"
    rfi_dada_file["TSAMP"] = f"{((1.0/bw) * nchan).to(u.us).value:.6f}"
    rfi_dada_file.utc_start = datetime.datetime.utcnow()
    rfi_dada_file.header.update(
        RFIProfile.generate_dada_header(gen_prof))
    rfi_dada_file.header["RFI_WIDTH_MEAN"] = format_param(gen_prof, mean_width)
    rfi_dada_file.header["RFI_WIDTH_STDDEV"] = format_param(gen_prof, sigma_width)
    rfi_dada_file.header["RFI_AMP_MEAN"] = f"{mean_amp.value:.3f}"
    rfi_dada_file.header["RFI_AMP_STDDEV"] = f"{sigma_amp.value:.3f}"
    rfi_dada_file.header["RFI_SPACING_MEAN"] = format_param(gen_prof, mean_spacing)
    rfi_dada_file.header["RFI_SPACING_STDDEV"] = format_param(gen_prof, sigma_spacing)
    rfi_dada_file.data = profile[:, :, np.newaxis]
    rfi_dada_file.dump_data()

    return rfi_file_path


def plot_rfi_profile(file_path: str):

    plt_file_path = os.path.splitext(file_path)[0] + ".pdf"

    rfi_dada_file = psr_formats.DADAFile(file_path).load_data()
    rfi_profile = rfi_dada_file.data[:, :, 0]

    nsamples = 50000
    fig, ax = plt.subplots(1, 1, figsize=(10, 6))

    freq = rfi_dada_file.centre_frequency
    bw = rfi_dada_file.bw
    nchan = rfi_profile.shape[1]

    imshow_kwargs = dict(
        aspect="auto",
        origin="lower",
        extent=(0, nsamples, (freq-bw/2).value, (freq+bw/2).value))

    ax.imshow(np.abs(rfi_profile[:nsamples, :]).T, **imshow_kwargs)
    ax.set_title("RFI Profile")
    ax.set_ylabel(f"Frequency ({freq.unit})")
    ax.set_xlabel("Sample Number")

    fig.savefig(plt_file_path)

    plt.show()




def main():
    parser = create_parser()

    parser.add_argument(
        "-p", "--plot", dest="plot",
        type=str,
        help="Plot an RFI profile")

    parser.add_argument(
        "-t", "--time", dest="time_or_samples",
        type=str, action=UnitAction,
        help="Specify the number of seconds of data to produce")

    parsed = parser.parse_args()

    level = logging.INFO
    if parsed.verbose:
        level = logging.DEBUG

    logging.basicConfig(level=level)

    if parsed.plot:

        plot_rfi_profile(parsed.plot)
        return

    options = dict(
        time_or_samples=parsed.time_or_samples,
        bw=parsed.bw,
        centre_freq=parsed.centre_freq,
        rfi_bw=parsed.rfi_bw,
        rfi_centre_freq=parsed.rfi_centre_freq,
        mean_width=parsed.mean_width,
        sigma_width=parsed.sigma_width,
        mean_amp=parsed.mean_amp,
        sigma_amp=parsed.sigma_amp,
        mean_spacing=parsed.mean_spacing,
        sigma_spacing=parsed.sigma_spacing,
        nchan=parsed.nchan
    )

    if parsed.config_file_path:
        options.update(load_config(parsed.config_file_path))

    rfi_file_path = create_rfi_profile(
        **options,
        output_dir=parsed.output_dir
    )

    plot_rfi_profile(rfi_file_path)


if __name__ == "__main__":
    main()
