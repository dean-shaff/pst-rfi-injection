import os
import logging
import typing

import astropy.units as u
import numpy as np
import matplotlib.pyplot as plt
import psr_formats

from rfi_injection import RFIProfile, RFIProfileInjector, NumQuantity
from rfi_injection.cli import create_parser, load_config



def create_product_existing_rfi(
    data_file_path: str,
    rfi_file_path: str,
    op_name: str = None
) -> typing.Tuple[np.ndarray]:

    op = None
    ending = ".rfi.dump"

    if op_name == "zero":
        def op(a, b):
            a[b != 0] = 0.0
            return a
        ending = ".rfi_zero.dump"

    dada_file = psr_formats.DADAFile(data_file_path).load_data()
    data = dada_file.data.copy()
    npol = data.shape[-1]

    rfi_dada_file = psr_formats.DADAFile(rfi_file_path).load_data()

    if (
        float(dada_file["BW"]) != float(rfi_dada_file["BW"]) or
        float(dada_file["FREQ"]) != float(rfi_dada_file["FREQ"])
    ):
        raise RuntimeError("Can't add RFI when BW and FREQ are different")

    rfi_prof = rfi_dada_file.data.copy()[:, :, 0, 0]
    nsamples_downsample, nchan = rfi_prof.shape[:2]
    nsamples = nsamples_downsample * nchan

    if nsamples < data.shape[0]:
        data = data[:nsamples, :, :]
    else:
        nsamples = data.shape[0]
        nsamples_downsample = nsamples // nchan
        nsamples = nsamples_downsample * nchan
        rfi_prof = rfi_prof[:nsamples_downsample, :]
        data = data[:nsamples, :, :]

    data_rfi = np.zeros([nsamples, 1, npol], dtype=data.dtype)
    fft_data = np.zeros(
        [nsamples_downsample, nchan, npol], dtype=data.dtype)
    fft_data_rfi = np.zeros(fft_data.shape, dtype=fft_data.dtype)

    for ipol in range(npol):
        injector = RFIProfileInjector(data[:, :, ipol], nchan, op=op)

        fft_signal = next(injector)
        fft_signal_with_rfi = injector.send(rfi_prof)
        ifft_signal = next(injector)

        data_rfi[:, 0, ipol] = ifft_signal
        fft_data[:, :, ipol] = fft_signal
        fft_data_rfi[:, :, ipol] = fft_signal_with_rfi

    new_file_path = os.path.splitext(data_file_path)[0] + ending

    new_dada_file = psr_formats.DADAFile(new_file_path)
    new_dada_file.header = dada_file.header
    new_dada_file.data = data_rfi
    new_dada_file.dump_data()

    return data, data_rfi, fft_data, fft_data_rfi


def create_product(
    data_file_path: str,
    *,
    nchan: int,
    rfi_bw: NumQuantity,
    rfi_centre_freq: NumQuantity,
    mean_width: NumQuantity,
    sigma_width: NumQuantity,
    mean_amp: NumQuantity,
    sigma_amp: NumQuantity,
    mean_spacing: NumQuantity,
    sigma_spacing: NumQuantity
) -> typing.Tuple[np.ndarray]:

    dada_file = psr_formats.DADAFile(data_file_path).load_data()
    data = dada_file.data.copy()
    npol = data.shape[-1]

    bw = float(dada_file["BW"]) * u.MHz
    centre_freq = float(dada_file["FREQ"]) * u.MHz

    def param_generator(mean, sigma):
        return lambda: np.random.randn(1) * sigma + mean

    gen_prof = RFIProfile(
        bw, centre_freq, nchan, rfi_bw, rfi_centre_freq,
        param_generator(mean_width, sigma_width),
        param_generator(mean_amp, sigma_amp),
        param_generator(mean_spacing, sigma_spacing)
    )

    injector = RFIProfileInjector(data[:, :, 0], nchan)
    prof = gen_prof(injector.nsamples_downsample)

    data_rfi = np.zeros([injector.clipped, 1, npol], dtype=data.dtype)
    fft_data = np.zeros(
        [injector.nsamples_downsample, nchan, npol], dtype=data.dtype)
    fft_data_rfi = np.zeros(fft_data.shape, dtype=fft_data.dtype)

    for ipol in range(npol):
        injector = RFIProfileInjector(data[:, :, ipol], nchan)

        fft_signal = next(injector)
        fft_signal_with_rfi = injector.send(prof)
        ifft_signal = next(injector)

        data_rfi[:, 0, ipol] = ifft_signal
        fft_data[:, :, ipol] = fft_signal
        fft_data_rfi[:, :, ipol] = fft_signal_with_rfi

    new_file_path = os.path.splitext(data_file_path)[0] + ".rfi.dump"

    new_dada_file = psr_formats.DADAFile(new_file_path)
    new_dada_file.header = dada_file.header
    new_dada_file.data = data_rfi
    new_dada_file.dump_data()

    rfi_file_name = f"rfi.profile.dump"
    rfi_dada_file = psr_formats.DADAFile(rfi_file_name)
    rfi_dada_file.header = dada_file.header
    rfi_dada_file.header.update(
        RFIProfile.generate_dada_header(gen_prof))
    rfi_dada_file.header["RFI_WIDTH_MEAN"] = f"{gen_prof.sample_to_time(mean_width).value:.6f}"
    rfi_dada_file.header["RFI_WIDTH_STDDEV"] = f"{gen_prof.sample_to_time(sigma_width).value:.6f}"
    rfi_dada_file.header["RFI_AMP_MEAN"] = f"{mean_amp.value:.0f}"
    rfi_dada_file.header["RFI_AMP_STDDEV"] = f"{sigma_amp.value:.0f}"
    rfi_dada_file.header["RFI_SPACING_MEAN"] = f"{gen_prof.sample_to_time(mean_spacing).value:.6f}"
    rfi_dada_file.header["RFI_SPACING_STDDEV"] = f"{gen_prof.sample_to_time(sigma_spacing).value:.6f}"
    rfi_dada_file.data = prof[:, :, np.newaxis]
    rfi_dada_file.dump_data()

    return data, data_rfi, fft_data, fft_data_rfi


def plot_product(
    data: np.ndarray,
    data_rfi: np.ndarray,
    fft_data: np.ndarray,
    fft_data_rfi: np.ndarray,
    plot_file_path: str = "rfi_added.png"
):
    fig, axes = plt.subplots(2, 2, figsize=(10, 8))

    axes[0, 0].plot(np.abs(data))
    axes[0, 0].set_title("Original Signal")
    axes[0, 0].set_ylabel("Amplitude")
    axes[0, 0].grid(True)

    axes[1, 0].plot(np.abs(data_rfi))
    # axes[1, 0].plot(np.abs(data_rfi[:, :, -1]))
    axes[1, 0].set_title("With impulsive RFI")
    axes[1, 0].set_ylabel("Amplitude")
    axes[1, 0].set_xlabel("Sample Number")
    axes[1, 0].grid(True)

    imshow_kwargs = dict(aspect="auto", origin="lower")

    axes[0, 1].imshow(np.abs(fft_data.T), **imshow_kwargs)
    axes[0, 1].set_title("Original Dynamic Spectrum")
    axes[0, 1].set_ylabel("Channel Number")

    axes[1, 1].imshow(np.abs(fft_data_rfi.T), **imshow_kwargs)
    axes[1, 1].set_title("Dynamic Spectrum with impulsive RFI")
    axes[1, 1].set_ylabel("Channel Number")
    axes[1, 1].set_xlabel("Sample Number")

    fig.tight_layout()
    fig.savefig(plot_file_path)


def main():

    parser = create_parser(
        description="Add RFI to an existing DADA file")

    parser.add_argument(
        "-i", "--in-file",
        dest="file_path", required=True,
        help=("Indicate the file to which RFI will be added. "
              "This file will override any provided bandwidth "
              "or centre frequency values"))

    parser.add_argument(
        "-ri", "--rfi-in-file",
        dest="rfi_file_path", required=False,
        help=("Add an existing RFI profile to the dataset"))

    parsed = parser.parse_args()

    level = logging.INFO
    if parsed.verbose:
        level = logging.DEBUG

    logging.basicConfig(level=level)
    logging.getLogger("matplotlib").setLevel(logging.ERROR)

    if parsed.rfi_file_path:
        products = create_product_existing_rfi(
            parsed.file_path, parsed.rfi_file_path#, op_name="zero"
        )
    else:
        options = dict(
            rfi_bw=parsed.rfi_bw,
            rfi_centre_freq=parsed.rfi_centre_freq,
            mean_width=parsed.mean_width,
            sigma_width=parsed.sigma_width,
            mean_amp=parsed.mean_amp,
            sigma_amp=parsed.sigma_amp,
            mean_spacing=parsed.mean_spacing,
            sigma_spacing=parsed.sigma_spacing,
            nchan=parsed.nchan
        )

        if parsed.config_file_path:
            options.update(load_config(parsed.config_file_path))

        products = create_product(parsed.file_path, **options)
    products = [arr[:, :, 0] for arr in products]
    plot_product(*products)
    plt.show()


if __name__ == '__main__':
    main()
