import typing
import logging

import numpy as np
import astropy.units as u

from .util import auto_property, NumQuantity

__all__ = [
    "RFIProfile",
    "RFIProfileInjector"
]

module_logger = logging.getLogger(__name__)


def _get_freq_range(
    bw: u.Quantity,
    centre_frequency: u.Quantity
) -> typing.List[u.Quantity]:
    return [centre_frequency - bw/2, centre_frequency + bw/2]


def _make_quantity(val: NumQuantity, default_unit: u.Unit):
    if hasattr(val, "value"):
        return val
    else:
        return val * default_unit


class RFIProfile:
    def __init__(self,
                 bw: NumQuantity,
                 centre_frequency: NumQuantity,
                 nchan: int,
                 rfi_bw: NumQuantity,
                 rfi_centre_frequency: NumQuantity,
                 width_generator: typing.Callable,
                 amp_generator: typing.Callable,
                 spacing_generator: typing.Callable):

        self._bw = _make_quantity(bw, u.MHz)
        self._centre_frequency = _make_quantity(centre_frequency, u.MHz)
        self._nchan = nchan
        self._rfi_bw = _make_quantity(rfi_bw, u.MHz)
        self._rfi_centre_frequency = _make_quantity(rfi_centre_frequency, u.MHz)

        self.width_generator = width_generator
        self.amp_generator = amp_generator
        self.spacing_generator = spacing_generator

        self._time_per_sample = (1.0 / bw * nchan).to(u.s)
        self._freq_range = _get_freq_range(bw, centre_frequency)
        self._freqs = np.linspace(
            self._freq_range[0], self._freq_range[-1], nchan)
        self._chans = np.arange(nchan)

        self._rfi_freq_range = _get_freq_range(rfi_bw, rfi_centre_frequency)
        self._rfi_chan_range = [
            self._chans[self._freqs > self._rfi_freq_range[0]][0],
            self._chans[self._freqs < self._rfi_freq_range[1]][-1]
        ]
        self._rfi_chan_range_slice = slice(*self._rfi_chan_range)

    def __call__(self, duration: typing.Union[int, u.Quantity]) -> np.ndarray:

        if hasattr(duration, "value"):
            nsamples = self.time_to_sample(duration)
        else:
            nsamples = duration
        prof = np.zeros((nsamples, self._nchan))

        current_sample = 0

        while current_sample < nsamples:

            next_sample = self.time_to_sample(self.spacing_generator())
            amplitude = self.amp_generator().value[0]
            width = self.time_to_sample(self.width_generator())

            if next_sample <= 0 or width <= 0:
                continue

            module_logger.debug(
                (f"RFIProfile.__call__: current_sample={current_sample}, "
                 f"next_sample={next_sample}, "
                 f"amplitude={amplitude}, width={width}"))
            current_sample += next_sample
            prof[current_sample: current_sample + width,
                 self._rfi_chan_range_slice] = amplitude
            current_sample += width

        return prof

    def time_to_sample(self, t: typing.Union[u.Quantity, int, float]):
        if hasattr(t, "value"):
            return int((t / self._time_per_sample).value)
        else:
            return int(t)

    def sample_to_time(self, sample: typing.Union[u.Quantity, int, float]):
        if hasattr(sample, "value"):
            return sample.to(u.s)
        else:
            return sample * self._time_per_sample

    def _reset(self):

        self._time_per_sample = (1.0 / self._bw * self._nchan).to(u.s)
        self._freq_range = _get_freq_range(self._bw, self._centre_frequency)
        self._freqs = np.linspace(
            self._freq_range[0], self._freq_range[-1], self._nchan)
        self._chans = np.arange(self._nchan)

        self._rfi_freq_range = _get_freq_range(
            self._rfi_bw, self._rfi_centre_frequency)
        self._rfi_chan_range = [
            self._chans[self._freqs > self._rfi_freq_range[0]][0],
            self._chans[self._freqs < self._rfi_freq_range[1]][-1]
        ]
        self._rfi_chan_range_slice = slice(*self._rfi_chan_range)

    @staticmethod
    def generate_dada_header(rfi_obj):
        header = {
            "RFI_BW": f"{rfi_obj.rfi_bw.value:.0f}",
            "RFI_FREQ": f"{rfi_obj.rfi_centre_frequency.value:.4f}"
        }

        return header


def custom_setter(obj, underscore_name, value):
    setattr(obj, underscore_name, value)
    obj._reset()


RFIProfile = auto_property([
    "bw",
    "centre_frequency",
    "nchan",
    "rfi_bw",
    "rfi_centre_frequency",
    "time_per_sample",
    "freq_range",
    "freqs",
    "chans",
    "rfi_freq_range",
    "rfi_chan_range",
    "rfi_chan_range_slice"
], custom_setter)(RFIProfile)


class RFIProfileInjector:

    def __init__(self, data, nchan, op=None):

        self.data = data
        self.nchan = nchan
        self.prof = None

        self.nsamples = self.data.shape[0]
        self.nsamples_downsample = self.nsamples // self.nchan
        self.clipped = self.nsamples_downsample * self.nchan
        self.data_down_fft = None
        self._iterator = self._iterator_factory()
        self.op = op
        if self.op is None:
            self.op = np.ndarray.__add__


    def __iter__(self):
        return self

    def __next__(self):
        return next(self._iterator)

    def send(self, *args):
        return self._iterator.send(*args)

    def _iterator_factory(self):

        def iterator(obj):
            data_down = obj.data[:obj.clipped].reshape(
                (obj.nsamples_downsample, obj.nchan))

            obj.data_down_fft = np.fft.fft(data_down, axis=1)
            prof = yield obj.data_down_fft.copy()
            if prof is not None:
                obj.data_down_fft = self.op(obj.data_down_fft, prof)
                # obj.data_down_fft += prof

            yield obj.data_down_fft.copy()
            yield np.fft.ifft(
                obj.data_down_fft, axis=1).reshape((obj.clipped, ))

        return iterator(self)
