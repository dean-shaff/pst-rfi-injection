import astropy.units as u

import toml

from .create_parser import create_parser, UnitAction


def load_config(config_file_path: str) -> dict:
    with open(config_file_path, "r") as fd:
        config = toml.load(fd)

    for key in config:
        if hasattr(config[key], "__contains__"):
            if "value" in config[key]:
                config[key] = (config[key]["value"] *
                               getattr(u, config[key]["unit"]))

    return config


__all__ = [
    "load_config",
    "create_parser",
    "UnitAction"
]
