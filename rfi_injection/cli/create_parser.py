import argparse

import astropy.units


__all__ = [
    "UnitAction",
    "create_parser"
]


class UnitAction(argparse.Action):

    def __call__(self, parser, namespace, values, option_string=None):
        idx = len(values)
        for c in values[::-1]:
            try:
                float(c)
                break
            except ValueError:
                idx -= 1

        value = values[:idx]
        unit = values[idx:]
        value = float(value)
        if unit != "":
            value = value * getattr(astropy.units, unit)

        setattr(namespace, self.dest, value)


def create_parser(**kwargs: str) -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(**kwargs)

    parser.add_argument("-v", "--verbose",
                        dest="verbose", action="store_true")

    parser.add_argument("-c", "--config-file",
                        dest="config_file_path", required=False)

    parser.add_argument("-o", "--output-dir",
                        default="./",
                        dest="output_dir", required=False)

    parser.add_argument("-b", "--bandwidth",
                        dest="bw",
                        type=str,
                        action=UnitAction,
                        help="Specify bandwidth, in MHz")

    parser.add_argument("-f", "--centre-freq",
                        dest="centre_freq",
                        type=str,
                        action=UnitAction,
                        help="Specify centre frequency, in MHz")

    parser.add_argument("-rb", "--rfi-bandwidth",
                        dest="rfi_bw",
                        type=str,
                        action=UnitAction,
                        help="Specify RFI bandwidth, in MHz")

    parser.add_argument("-rf", "--rfi-centre-freq",
                        dest="rfi_centre_freq",
                        type=str,
                        action=UnitAction,
                        help="Specify RFI centre frequency, in MHz")

    parser.add_argument("-ch", "--channels",
                        dest="nchan",
                        type=int,
                        help=("Specify number of RFI profile "
                              "channels to generate"))

    parser.add_argument("-mw", "--mean-width",
                        dest="mean_width", type=str,
                        action=UnitAction,
                        help=("Specify the mean width of RFI impulse, "
                              "in seconds or samples"))

    parser.add_argument("-sw", "--sigma-width",
                        dest="sigma_width", type=str,
                        action=UnitAction,
                        help=("Specify the standard deviation of the width of "
                              "the RFI impulse, in seconds or samples"))

    parser.add_argument("-ma", "--mean-amp",
                        dest="mean_amp", type=str,
                        action=UnitAction,
                        help=("Specify the mean amplitude of RFI impulses,"
                              " in units of voltage"))

    parser.add_argument("-sa", "--sigma-amp",
                        dest="sigma_amp", type=str,
                        action=UnitAction,
                        help=("Specify the standard deviation of the amplitude "
                              "of RFI impulses, in units of voltage"))

    parser.add_argument("-ms", "-mean-spacing",
                        dest="mean_spacing", type=str,
                        action=UnitAction,
                        help=("Specify the mean spacing between RFI "
                              "impulses, in samples or seconds"))

    parser.add_argument("-ss", "--sigma-spacing",
                        dest="sigma_spacing", type=str,
                        action=UnitAction,
                        help=("Specify the standard deviation of spacing "
                              "between RFI impulses, in samples or seconds"))

    return parser
