import typing

import astropy.units

NumQuantity = typing.Union[int, float, astropy.units.Quantity]


class auto_property:

    def __init__(self, properties, custom_setter=None):
        self.properties = properties
        self.custom_setter = custom_setter

    def __call__(self, cls):

        def getter_factory(underscore_name):
            def fget(obj):
                return getattr(obj, underscore_name)
            return fget

        def setter_factory(underscore_name):
            def fset(obj, value):
                if self.custom_setter is None:
                    setattr(obj, underscore_name, value)
                else:
                    self.custom_setter(obj, underscore_name, value)
            return fset

        for name in self.properties:

            underscore_name = f"_{name}"
            setattr(cls, name, property(
                fget=getter_factory(underscore_name),
                fset=setter_factory(underscore_name)))

        return cls
